<!DOCTYPE html>
<html lang="en">

<?php

require __DIR__."\util\connect.php";

$result = sqlsrv_query($conn, "SELECT * FROM barang");

?>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="asset/style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>Input Stok</title>
</head>

<body>
    <?php
    session_start();
    if(isset($_SESSION['status'])){
        if($_SESSION['status'] == "sukses"){
            echo "<div id=\"asukses\" class=\"alert alert-success\" role=\"alert\"><center>OPERASI SUKSES</center></div>";
        }else{
            echo "<div id=\"agagal\" class=\"alert alert-danger\" role=\"alert\"><center>OPERASI GAGAL</center></div>";
        }
    }
    session_unset();
    session_destroy();
    ?>
    <div class="container">
        <button class="btn btn-primary" data-toggle="modal" data-target="#myModal">+ Tambah Barang</button>
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th>
                            No.
                        </th>
                        <th>
                            Nama
                        </th>
                        <th>
                            Jumlah Stok
                        </th>
                        <th>
                            Satuan
                        </th>
                        <th>
                            Update Terakhir
                        </th>
                        <th>
                            Action
                        </th>
                    </tr>
                </thead>
                <tbody>

                <?php

                $nomer = 1;

                while($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)){
                    echo "<tr>";
                    echo "<td>";
                    echo $nomer++;
                    echo "</td>";
                    echo "<td>";
                    echo $row['nama'];
                    echo "</td>";
                    echo "<td>";
                    echo $row['stok'];
                    echo "</td>";
                    echo "<td>";
                    echo $row['satuan'];
                    echo "</td>";
                    echo "<td>";
                    echo $row['lastupdate'];
                    echo "</td>";
                    echo "<td>";
                    echo "<a href=\"?id=".$row['id']."\" class=\"btn btn-primary btn-circle btn-md\">
                    <ion-icon name=\"create\"></ion-icon>
                </a>
                <a href=\"action/delete.php?idstok=".$row['id']."\" class=\"btn btn-danger btn-circle btn-md\">
                    <ion-icon name=\"trash\"></ion-icon>
                </a>";
                    echo "</td>";
                    echo "</tr>";
                }

                ?>
                </tbody>
            </table>
        </div>


        <!-- Modal -->
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Tambah Barang</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="action/create.php">
                            <div class="form-group">
                                <label for="nama">Nama Barang</label>
                                <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama Barang">
                            </div>
                            <div class="form-group">
                                <label for="jumlah">Jumlah Stok</label>
                                <input type="number" class="form-control" id="jumlah" name="jumlah"
                                    placeholder="Jumlah Stok Barang">
                            </div>
                            <div class="form-group">
                                <label for="satuan">Satuan</label>
                                <input type="text" class="form-control" id="satuan" name="satuan"
                                    placeholder="Satuan Stok Barang">
                            </div>
                            <button type="submit" class="btn btn-primary btn-block">Save</button>
                            <button type="button" class="btn btn-secondary btn-block"
                                data-dismiss="modal">Cancel</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div id="editModal" class="modal fade" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Edit Barang</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="action/update.php">

                        <?php

                        if(isset($_GET['id'])){
                            $id = $_GET['id'];
                            $edit = sqlsrv_query($conn, "SELECT * FROM barang WHERE id='$id'");

                            while($row = sqlsrv_fetch_array($edit, SQLSRV_FETCH_ASSOC)){
                                
                                echo "<input type=\"hidden\" class=\"form-control\" id=\"idstok\" name=\"idstok\" value=\"".$id."\">
                                <div class=\"form-group\">
                                <label for=\"nama\">Nama Barang</label>
                                <input type=\"text\" class=\"form-control\" id=\"nama\" name=\"nama\" value=\"".$row['nama']."\" placeholder=\"Nama Barang\">
                                </div>
                                <div class=\"form-group\">
                                <label for=\"jumlah\">Jumlah Stok</label>
                                <input type=\"number\" class=\"form-control\" id=\"jumlah\" name=\"jumlah\" value=\"".$row['stok']."\" placeholder=\"Jumlah Stok Barang\">
                                </div>
                                <div class=\"form-group\">
                                <label for=\"satuan\">Satuan</label>
                                <input type=\"text\" class=\"form-control\" id=\"satuan\" name=\"satuan\" value=\"".$row['satuan']."\" placeholder=\"Satuan Stok Barang\">
                                </div>";
                            }
                        }

                        ?>
                            <button type="submit" class="btn btn-primary btn-block">Save</button>
                            <button type="button" class="btn btn-secondary btn-block"
                                data-dismiss="modal">Cancel</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
</body>

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
    integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
</script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
    integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
    integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
</script>
<script src="https://unpkg.com/ionicons@4.5.10-0/dist/ionicons.js"></script>
<?php
if(isset($_GET['id'])){
    ?>
    <script>
    $(document).ready(function(){
        $('#editModal').modal('show');
    })
    </script>
    <?php
}
?>

<script>

$(document).ready(function(){
   setTimeout(function() {
        $(".alert").alert('close');
    }, 2000);
})

</script>

</html>